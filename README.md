### LCS - SAT solver
---

Here you can find an implementation of a SET solver that was created as a part
of a Logic in Computer Science course at the Faculty of Mathematics and Physics
\- University of Ljubljana
