(* grammer := p | np | (p & p) | (p V p) | (p imp p) *)

(* variable *)
type variable = int 

(* definition of expression *)
datatype EXPR =
  P of VAR |
  NOT of P |
  AND of EXPR * EXPR |
  OR of EXPR * EXPR |
  IMP of EXPR * EXPR;

infix 1 AND;
infix 2 OR;
infix 3 IMP;

(* Evaluate the expression. *)
fun evaluate(expr: EXPR, variables: list): bool =
  case expr of
    P x => #value x |
    NOT x => (not evaluate(x))|
    x AND y => evaluate(x) andalso evaluate(y) |
    x OR y => evaluate(x) orelse evaluate(y) |
    x IMP y => (not (evaluate(y))) orelse evaluate(x);

type CNF = VAR list list;

datatype CNF =
  LITELAR of P |
  CLAUSE of CNF list |
  FORMULA of CNF list;

fun simplifyCNF(expr: CNF, var: VAR): CNF option =
  case expr of
    LITELAR x =>
      if (#mame var = #name x)
      else
          SOME x

(* list of variables *)
(* type VARS = bool list; *)

(* DPLL *)
(* fun DPLL(expr: EXPR): VARS = *)

(* P{name="x", value=false} AND P{name="y", value=true} *)


