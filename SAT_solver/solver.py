from expression import *

class Solver:
    """ SAT solver"""

    class Status:
        """ Indicator if a function is satisfiable. """
        
        def __init__(self, satisfiable, valuation = []):
            """ Initialize
            
            Arguments:
            ----------
            satisfiable: boolean
                a boolean value indicating if function is satisfiable

            valuation: [Expression]
                a list of variables (Not(Var) or Var) that satisfy the formula
            """

            self.satisfiable = satisfiable
            self.valuation = valuation

        def isSatisfiable(self):
            return self.satisfiable

        def __str__(self):
            return \
                "satisfiable = " + str(self.satisfiable) + ", " \
                "valuation = " + str([str(x) for x in self.valuation])

    def solve(self, formula, assumedTrue = [], recLevel = 0):
        """Find a valuation that satisfies a formula in CNF form.

        The function uses the DPLL algorithm.
        
        Arguments
        ---------
        formula: CNF
            a formula for which a valuation is searched for
        assumedTrue: [string]
            a list of literals that are assumed to be true
        recLevel: int
            recursion level

        Returns:
        -------
        (true, [string])
            a boolean value indicating if a valuation was found and the
            valuation - If a valuation is not found, the boolean value is
            false, else the boolean value is true.
        """

        # Print info
        # print("Level " + str(recLevel))
        # print("> assumedTrue = " + str([str(x) for x in assumedTrue]))
        # print("> formula = " + str(formula))

        # Copy the formula.
        simFormula = CNF(*formula.exprs)

        if len(assumedTrue) > 0:
            # print("> assumedTrue = " + str(assumedTrue[-1]))

            # Simplify the formula with the last litral in assumedTrue list.
            try:
                simFormula = self.__simplifyBy(simFormula, assumedTrue[-1])
            except CannotBeSatisfied:
                #2. Check if solvable or unsolvable.

                # The function contains an empty clause.
                # Thus, the formula is unsolvable.
                return Solver.Status(False, assumedTrue)

            # print("> simFormula = " + str(simFormula))

        #2. Check if solvable or unsolvable.
        if str(simFormula) == "T":
            # The function is equal to '[]'. Thus, formula is solvable.
            return Solver.Status(True, assumedTrue)
        # elif str(simFormula) == "F":
        #     # TODO: Is this case really needed?

        #     # The function is equal to '[()]'. It contains an empty clause.
        #     # Thus, the formula is unsolvable.
        #     return Solver.Status(False, assumedTrue)

        #1. Find a unit clause and simplify the formula.
        unitClauses = simFormula.getUnitClauses()
        for unitClause in unitClauses:
            ret = self.solve(
                simFormula,
                assumedTrue + [unitClause],
                recLevel + 1)
            if ret.isSatisfiable():
                return ret

        # 4. Select a new literal. Assume it is true.
        literals = set([Var(x) for x in simFormula.getPresentVariables()])
        variableLiterals = literals.symmetric_difference(unitClauses)
        for literal in list(literals):
            ret = self.solve(
                simFormula,
                assumedTrue + [Variable(literal)],
                recLevel + 1)
            if ret.isSatisfiable():
                return ret

            # If not solvable select another literal.

        # The function is not solvable.
        return Solver.Status(False, assumedTrue)

    def __simplifyBy(self, cnf, l):
        """ Simplify a CNF formula by a literal l.
        
        The formula removes a literal !l from the clauses or
        removes the entire clauses if it contains literal l.

        Arguments
        ---------
        cnf: CNF
            A logical formula in CNF form.
        l: Not or Not(Variable)
            The variable by which the formula is simplified.

        Returns
        -------
        CNF
            Returns a simplified CNF formula.
        """

        def simplifyClaus(clause, l):
            """Simplify a clause.
            
            If clause contains !l literal, remove the literal.
            If clause contains l literal, remove the clause. None is returns.
            """

            simClause = []
            for x in clause.exprs:
                # Remove the clause if l is present.
                if x == l:
                    return None

                # Remove the !l litelars.
                elif type(x) is Not:
                    if type(l) is Variable and x.expr == l:
                        continue
                elif type(x) is Variable:
                    if type(l) is Not and x == l.expr:
                        continue
                
                # Invalid combination.
                else:
                    raise InvalidType

                simClause.append(x)

            return Or(*simClause)

        simFormula = []
        for cl in cnf.exprs:
            simCl = simplifyClaus(cl, l)
    
            if simCl == None:
                # The clause was removed.
                continue
            elif str(simCl) == "F":
                # The function cannot be satisfied.
                # One of the clauses is false.
                raise CannotBeSatisfied  

            simFormula.append(simCl)

        return CNF(*simFormula)

