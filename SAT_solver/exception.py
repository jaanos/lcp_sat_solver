class ExpException(RuntimeError):
    pass

class InvalidType(ExpException):
    pass

class InvalidArgument(ExpException):
    pass

class CannotBeSatisfied(ExpException):
    pass
