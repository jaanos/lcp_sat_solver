"""
Find a validation for a CNF formula.

The program reads a CNF formula from a file and finds a validation
that satisfies the formula.

by: zputrle
course: LCS (Logic in Computer Science)
"""

# TODO: Add a logging system that will allow to change the level of logging.

from expression import *
from solver import Solver
from iofile import fromFile
from iofile import toFile

import argparse
import os
import datetime

import time

def status2Values(status):
    values = {}
    for y in status.valuation:
        if type(y) == Not:
            key = y.expr.x
            val = 0
        else:
            key = y.x
            val = 1

        # Check if all the variables in the valuation are unique.
        if key in values.keys():
            raise InvalidArgument(
                "A variable appears twice in the valuation. "
                "variable = " + str(key))

        values[key] = val

    return values

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description = __doc__)
    parser.add_argument("file_path",
        help="a path to the file that contains a CNF formula in DIMACS format")
    parser.add_argument("-o", "--output",
        help="a path to a file where the valuation is stored")
    args = parser.parse_args()

    args.output = \
        os.path.splitext(os.path.basename(args.file_path))[0] + \
        "_valuation_" + datetime.datetime.now().strftime("%Y%m%dT%H%M%S") + \
        ".txt"


    print("Arguments")
    print("---------")
    print("file_path = " + args.file_path)
    print("output = " + args.output)
    print("")

    # Get a formula from a file.
    dmFile = fromFile(args.file_path)

    print("File info")
    print("---------")
    print("path = " + dmFile.path)
    print("nbVar = " + str(dmFile.nbVar))
    print("nbClauses = " + str(dmFile.nbClauses))
    print("")

    start = time.time()

    # Find a solution to a formula.
    solver = Solver()
    ret = solver.solve(dmFile.formula)

    end = time.time()

    print("Resoult")
    print("-------")
    print("time_spent = " + str(end - start) + "s")
    print("valuation_size = " + str(len(ret.valuation)))
    print(ret)
    print("evaluating_function = " + str(dmFile.formula.evaluate(status2Values(ret))))
    print("")

    # Store to file.
    print("The valuation was stored to file '" + args.output + "'.")
    if ret.isSatisfiable():
        toFile(ret.valuation, args.output) 
    else:
        print("Storing an empty valuation ([0]).")
        toFile([0], args.output)
