#!/bin/bash

if [[ $# != 2 ]]
then
    echo "Error: Invalid arguments"
    exit 1
fi

echo "Arguments"
echo "---------"
echo "PATH_1 = $1"
echo "PATH_2 = $2"
echo ""

# Sort.
cat $1 | tr ' ' '\n' | sort -n > TMP_1.txt
cat $2 | tr ' ' '\n' | sort -n > TMP_2.txt

# Compare.
diff TMP_1.txt TMP_2.txt > /dev/null

if [[ $? == 0 ]];
then
    echo "The files are the same."
else
    echo "The files are NOT the same."
fi

echo ""
