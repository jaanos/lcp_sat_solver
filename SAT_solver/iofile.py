from expression import *
from solver import Solver
from collections import namedtuple

def DIMACSClause2Expression(DIMACSClause):
    variables = []
    splitClause = [int(x) for x in DIMACSClause.split()]
    # Check that the DIMACS clause is not empty and
    # that the last number is always 0.
    if len(splitClause) == 0 or not splitClause[-1] == 0:
        raise InvalidArgument(
            "A DIMACS clause is empty or the last number is not 0. " +
            "clause = " + str(DIMACSClause))

    for num in splitClause[:-1]:
        if num > 0:
            variables.append(Var("x" + str(num)))
        elif num < 0:
            variables.append(Not(Var("x" + str(num * -1))))
        else:
            raise InvalidArgument(
                "A DIMACS clause contains 0 that is not the last number. " +
                "clause = " + str(DIMACSClause))
        
    return Or(*variables) 

def fromFile(path):
    """Create a CNF formula from a DIMACS file.
    
    The function reads a file and returns a CNF formula. The content of a file
    is expected to be in a DIMACS format.

    Parameters
    ----------
    path : string
        a path to a file containing a CNF formula in DIMACS format

    Returns
    -------
    namedtuple("DIMACSFile",["path","nbVar","nbClauses","formula"])
        Returns a namedtuple representing the content of a DIMSE format.
    """
    
    # Open a file.
    fl = open(path, "r")
    lines = fl.readlines()

    i = 0
    # Ignore comments and empty lines. 
    for i in range(0, len(lines)):
        strLine = lines[i].strip()
        if len(strLine) == 0:
            # Ignore empty lines.
            continue
        elif strLine[0] == "c":
            # Ignore comments.
            continue
        else:
            break

    # Parse ID line.
    idLine = lines[i].split()
    if not (len(idLine) == 4 and idLine[0] == "p" and idLine[1] == "cnf"):
        raise InvalidArgument(
            "DIMACS ID line is not valid. " + 
            "ID line = " + idLine)

    nbVar = int(idLine[2])
    nbClauses = int(idLine[3])
   
    # Parse clauses.
    andExpressions = []
    for j in range(i + 1, len(lines)):
        strLine = lines[j].strip()
        if strLine[0] == "%":
            break
        andExpressions.append(DIMACSClause2Expression(lines[j]))
    
    DIMACSFile = namedtuple(
        "DIMACSFile",
        ["path",
         "nbVar",
         "nbClauses",
         "formula"]
    )

    return DIMACSFile(
        path=path,
        nbVar=nbVar,
        nbClauses=nbClauses,
        formula=CNF(*andExpressions))

def toFile(valuation, path):
    nums = []
    for val in valuation:
        num = str(val).strip('!x')

        if type(val) == Not:
            nums.append("-" + num)
        else:
            nums.append(num)

    fl = open(path, "w")
    fl.write(" ".join(nums))
