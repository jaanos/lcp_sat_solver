from exception import *

class Expression:
    def evaluate(self, values):
        pass

    # TODO: Implement.
    def simplify(self):
        return self

    # TODO: Implement.
    def tseytin(self, mapping):
        return self

    # TODO: Implement.
    def equiv(self, variable):
        return And(Or(variable, Not(self)), Or(Not(variable), self))

class Variable(Expression):

    def __init__(self, x):
        self.x = x

    def __str__(self, parentheses = False):
        return str(self.x)

    def __eq__(self, other):
        return str(self) == str(other)

    def __hash__(self):
        return hash(self.x)

    def getPresentVariables(self):
        return set([str(self)])

    def evaluate(self, values):
        return values[self.x]

# Variable alias
Var = Variable

class Not(Expression):
    """ NOT connector """ # TODO: Is this really a connector?

    connective = "!"

    def __init__(self, expr):
        self.expr = expr

    def __str__(self, parentheses = False):
        return self.connective + str(self.expr)

    def __hash__(self):
        return hash((self.connective, self.expr))

    def __eq__(self, other):
        return str(self) == str(other)

    def getPresentVariables(self):
        return self.expr.getPresentVariables()

    def evaluate(self, values):
        return not self.expr.evaluate(values)

class Multi(Expression):
    """Multiple expressions"""
    
    def __init__(self, *expr):
        self.exprs = exprs

    def __str__(self, parentheses = False):
        if len(self.exprs) == 0:
            return self.empty
        elif len(self.exprs) == 1:
            return self.exprs[0].__str__(parentheses)
        else: 
            out = \
                (" " + self.connective + " ").join(
                    x.__str__(True) for x in self.exprs)
            if parentheses:
                out = "(" + out + ")"
            
            return out

    def getPresentVariables(self):
        vrs = set()
        for expr in self.exprs:
            vrs = vrs.union(expr.getPresentVariables())
        return vrs

class And(Multi):
    """ END connector """

    connective = "&"
    empty = "T"
    
    def __init__(self, *exprs):
        self.exprs = exprs
    
    def evaluate(self, values):
        res = True
        for expr in self.exprs:
            res = res & expr.evaluate(values) 
        return res


class Or(Multi):
    """ OR connector """

    connective = "V"
    empty = "F"

    def __init__(self, *exprs):
        self.exprs = exprs

    def evaluate(self, values):
        for expr in self.exprs:
            try:
                evl = expr.evaluate(values)
            except KeyError:
                # If at leas on of the values in the expression is True
                # return True.
                if self._ifTrue(values):
                    return 1

                # Else, propagate the exception.
                raise

            # If one expression is True inside of an OR, the entire OR is True.
            # Thus, we can immediately return True. This way we do not need to
            # know all the values of all the variables.
            if evl == 1:
                return 1

        return 0

    def _ifTrue(self, values):
        """Check if at leas on of the values in the expression is True."""
        for expr in self.exprs:
            try:
                if expr.evaluate(values) == 1:
                    return True
            except KeyError:
                continue

        return False

class CNF(And):
    """Conjunctive Normal Form"""

    def __init__(self, *exprs):
        # Check that clauses are all Or expressions.
        if not all((type(cl) is Or) for cl in exprs):
            raise InvalidType(
                "A CNF formula could not be constructed. "
                "The 'exprs' argument has invalid format.")

        self.exprs = exprs

    def getUnitClauses(self):
        unitClauses = set()
        for clause in self.exprs:
            if len(clause.exprs) == 1:
                    unitClauses.add(clause.exprs[0])
        return unitClauses

