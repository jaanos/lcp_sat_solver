def tokenize(string):
    "Get tokens and ignore lines that start with #."

    tokens = []
    for line in string.splitlines():
        stripedLine = line.strip()
        # Ignore comments (lines starting with #) and empty lines.
        if len(stripedLine) == 0 or stripedLine[0] == "#":
            continue
        tokens.extend(line.split())

    return tokens

class Block:
    def __init__(self, tokens):
        self._index = 0
        self.tokens = tokens
    
    def isEmpty(self):
        return len(self.tokens) == 0;

    def isWord(self):
        return len(self.tokens) == 1;

    def getWord(self):
        return self.tokens[0]

    def __iter__(self):
        return self

    def __next__(self):
        if self._index >= len(self.tokens):
            raise StopIteration;
        block =  Block(self.tokens[self._index])
        self._index += 1
        return block

class TreeNode:
    def __init__(self, branch, leafs = []):
        self.branch = branch
        self.leafs = leafs

    def isLeaf(self):
        return len(leafs) == 0

    def draw2DTree(self, offset = 0):
        """Draw a 2D representation of a Tree. """

        string = "".join("  " for x in range(0, offset));
        if offset == 0:
            string += ">"
        else:
            string += "|--"
        string += " " + str(self.branch)

        for leaf in self.leafs:
            string += "\n" + leaf.draw2DTree(offset + 1)

        return string

# All available operators.
OPERATORS = {
    "AND": "&",
    "OR": "V"
}
OPS = OPERATORS

# Operator priority - Operators on the right side bind stronger then operators
# on the left side.
OPERATOR_PRIORITY = [OPS["OR"], OPS["AND"]]

def generateSyntaxTree(tokens):
    """ Generate a syntax tree.
     
    LL parser - From left to right. Left most derivative firs.
    Used grammar: exp -> exp and exp | exp or exp | var
    """
    if len(tokens) == 0:
        raise RuntimeError("Empty TreeNode")
    elif len(tokens) == 1:
        return TreeNode(tokens[0])
    else:
        for operator in OPERATOR_PRIORITY:
            for i in range(0, len(tokens)):
                if tokens[i] == operator:
                    return TreeNode(
                        tokens[i],
                        [generateSyntaxTree(tokens[:i]),
                         generateSyntaxTree(tokens[i+1:])]
                    )

    raise RuntimeError("Invalid string")

if __name__ == "__main__":
    #tree = TreeNode("&", [TreeNode("&", [TreeNode("u"), TreeNode("y")]), TreeNode("y")])

    for block in Block(tokenize("x y z")):
        if block.isEmpty() == False and block.isWord() == True:
            print(block.getWord())
    
    tokens = tokenize("x & x V y & x")
    print(tokens)
    tree = generateSyntaxTree(tokens)
    print(tree.draw2DTree())
